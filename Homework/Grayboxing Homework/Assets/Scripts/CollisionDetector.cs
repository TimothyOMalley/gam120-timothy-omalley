﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionDetector : MonoBehaviour
{
    public UnityEvent OnCollision = new UnityEvent();
    public UnityEvent OnEnterTrigger = new UnityEvent();
    public UnityEvent OnExitTrigger = new UnityEvent();
    
    public void OnCollisionEnter(Collision collision)
    {
        OnCollision.Invoke();
    }

    public void OnTriggerEnter(Collider other)
    {
        OnEnterTrigger.Invoke();
    }

    public void OnTriggerExit(Collider other)
    {
        OnExitTrigger.Invoke();
    }
}
