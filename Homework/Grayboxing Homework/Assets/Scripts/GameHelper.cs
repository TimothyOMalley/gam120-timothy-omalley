using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class GameHelper : MonoBehaviour
{
    public UnityEvent OnStart = new UnityEvent();

    void Start()
    {
        OnStart.Invoke();
    }
	
    public void DestroyObject(GameObject obj)
    {
        Destroy(obj);
    }
	
    public void LoadScene(string name)
    {
        SceneManager.LoadScene(name);
    }
}