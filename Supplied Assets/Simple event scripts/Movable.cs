﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Movable : MonoBehaviour
{
    private bool isMoving = false;

    private Vector3 oldPosition;
    private Vector3 oldScale;
    private Quaternion oldRotation;

    private Vector3 targetPosition;
    private Vector3 targetScale;
    private Quaternion targetRotation;

    private float moveDuration = 1f;
    private float moveTimer = 0f;

    public UnityEvent OnFinish = new UnityEvent();
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isMoving)
        {
            moveTimer -= Time.deltaTime;
            float t = 1f - moveTimer / moveDuration;
            transform.position = Vector3.Lerp(oldPosition, targetPosition, t);
            transform.localScale = Vector3.Lerp(oldScale, targetScale, t);
            transform.rotation = Quaternion.Slerp(oldRotation, targetRotation, t);
            if(moveTimer <= 0f)
            {
                isMoving = false;
                OnFinish.Invoke();
            }
        }
    }

    public void SetDuration(float duration)
    {
        moveDuration = duration;
    }

    public void Move(Transform where)
    {
        Move(where, false, moveDuration);
    }

    public void MoveRotate(Transform where)
    {
        MoveRotate(where, false, moveDuration);
    }

    public void Move(Transform where, bool relative, float duration)
    {
        isMoving = true;
        oldPosition = transform.position;
        oldScale = transform.localScale;
        oldRotation = transform.rotation;

        targetPosition = where.position;
        targetScale = oldScale;
        targetRotation = oldRotation;

        moveDuration = duration;
        moveTimer = moveDuration;

        if(relative)
        {
            targetPosition += oldPosition;
        }
    }

    public void MoveRotate(Transform where, bool relative, float duration)
    {
        isMoving = true;
        oldPosition = transform.position;
        oldScale = transform.localScale;
        oldRotation = transform.rotation;

        targetPosition = where.position;
        targetScale = oldScale;
        targetRotation = where.rotation;

        moveDuration = duration;
        moveTimer = moveDuration;

        if (relative)
        {
            targetPosition += oldPosition;
            targetRotation = targetRotation * oldRotation;
        }
    }

    public void MoveScale(Transform where, bool relative, float duration)
    {
        isMoving = true;
        oldPosition = transform.position;
        oldScale = transform.localScale;
        oldRotation = transform.rotation;

        targetPosition = where.position;
        targetScale = where.localScale;
        targetRotation = oldRotation;

        moveDuration = duration;
        moveTimer = moveDuration;

        if (relative)
        {
            targetPosition += oldPosition;
            targetScale += oldScale;
        }
    }

    public void MoveTransform(Transform where, bool relative, float duration)
    {
        isMoving = true;
        oldPosition = transform.position;
        oldScale = transform.localScale;
        oldRotation = transform.rotation;

        targetPosition = where.position;
        targetScale = where.localScale;
        targetRotation = where.rotation;

        moveDuration = duration;
        moveTimer = moveDuration;

        if (relative)
        {
            targetPosition += oldPosition;
            targetScale += oldScale;
            targetRotation = targetRotation * oldRotation;
        }
    }
}
