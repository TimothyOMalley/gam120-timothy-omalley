﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimedEvent : MonoBehaviour
{
    public UnityEvent OnTimeUp = new UnityEvent();

    public float delay = 4.0f;
    private float timer;

    public bool repeat = false;

    // Start is called before the first frame update
    void Start()
    {
        timer = delay;
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > 0f)
        {
            timer -= Time.deltaTime;
            if (timer <= 0f)
            {
                if (repeat)
                    timer += delay;
                OnTimeUp.Invoke();
            }
        }
    }
}
